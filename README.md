# time-travel-music-machine
Songs where they say the (current) year in the lyrics. 
Attribution is appreciated if you use this research.

### 1964
- Jim Nesbitt - Looking For More In '64

### 1965
- Jim Nesbitt - Still Alive in '65

### 1966
- Jim Nesbitt - Heck of a Fix in 66

### 1968
- Jim Nesbitt - Clean the Slate in '68

### 1969
- The Stooges - 1969

### 1970
- [The Stooges - 1970](https://genius.com/The-stooges-1970-lyrics) _h/t Dakota_

### 1971
- Jim Nesbitt - Havin' Fun in '71

### 1972
- Bob Seger - Back in '72

### 1979
- Adicts - Get Adicted

### 1980
- [Jimmy Spicer - Adventures of Super Rhyme (Rap)](https://genius.com/Jimmy-spicer-adventures-of-super-rhyme-rap-lyrics)

### 1981
- Public Image Ltd - 1981

### 1982
- Abrasive Wheels - 1982

### 1984
- 2 Live Crew - The Revelation
- The 4 Skins - 1984

### 1985
- Doug E. Fresh & The Get Fresh Crew - La Di Da Di

### 1986
- Eric B & Rakim - Eric B. is President

### 1987
- Boogie Down Productions - Super Hoe #4 (~4:18 original Man and His Music release)
- Boogie Down Productions - Bdp Meledy #11

### 1988
- Big Daddy Kane - Ain't no Half-Steppin
- [Boogie Down Productions - My Philosophy](http://genius.com/Boogie-down-productions-my-philosophy-lyrics)
- [Public Enemy - Don't Believe the Hype](http://genius.com/Public-enemy-dont-believe-the-hype-lyrics)

### 1989
- Public Enemy - Fight the Power
- [Geto Boys - Seek and Destroy](http://genius.com/Geto-boys-seek-and-destroy-lyrics)
- Digital Underground - Gutfest '89 (released March 1990)
- Operation Ivy - Freeze Up
- Fearless Iranians From Hell - Big Crime '89
- Sick of It All - Clobberin' Time / Pay the Price

### 1990
- Public Enemy - Reggie Jax
- [Geto Boys - Scarface](http://genius.com/Geto-boys-scarface-lyrics)
- [DJ Quik - Sweet Black Pussy](http://genius.com/Dj-quik-sweet-black-pussy-lyrics)
- [Too $hort - Rap Like Me](http://genius.com/Too-short-rap-like-me-lyrics)
- [Chubb Rock - Treat 'Em Right](https://genius.com/Chubb-rock-treat-em-right-lyrics)

### 1991
- NWA - Alwayz into somethin
- Geto Boys - We Can't Be Stopped
- Geto Boys - Ain't With Being Broke
- Geto Boys - The Other Level
- [2Pac - If My Homie Calls](https://genius.com/2pac-if-my-homie-calls-lyrics)
- Master P - Safe Sex (Street Mix)
- Master P - What's Up With That (Street Mix) (feat. Silkk The Shocker)
- Master P - Bitches and Money (Street Mix)
- Public Enemy - Power to the People

### 1992 
- A Tribe Called Quest - Scenario (remix)
- Dr. Dre - Fuck Wit Dre Day (And Everybody's Celebratin') 
- [Dr. Dre - Lyrical Gangbang](http://genius.com/331514) :: https://youtu.be/onaoNS_A2Og?t=2m48s
- Nas - Halftime
- [Pete Rock & C.L. Smooth - They Reminisce Over You (T.R.O.Y.)](http://genius.com/Pete-rock-and-cl-smooth-they-reminisce-over-you-troy-lyrics)
- Leaders of the New School - Connections - as heard on https://www.nts.live/shows/TED-DRAWS/episodes/ted-draws-15th-january-2019
- Main Source - Fakin the Funk
- Master P - Shoot 'em Up
- Master P - Psycho Rhymes
- Master P - Fuck A Bitch 'cuz I'm Paid
- Master P - Shouts
- [TLC - Ain’t 2 Proud 2 Beg](https://genius.com/Tlc-aint-2-proud-2-beg-lyrics)

### 1993
- WuTang - Can it all be so simple 
- Souls of Mischief - 93 'til Infinity
- Hieroglyphics - You Never Know
- [Too $hort - Playboy $hort (Get In Where You Fit In)](http://genius.com/Too-short-playboy-short-get-in-where-you-fit-in-lyrics) :: https://youtu.be/m0GFDgbvqGo?t=2m10s
- [Ice Cube - Check Yo Self ['The Message' Remix]](http://genius.com/Ice-cube-check-yo-self-the-message-remix-lyrics) ::  https://youtu.be/K-LdCBSv4Lk?t=3m42s
- [Snoop Dogg - Who Am I (What's My Name)?](http://genius.com/85610)
- Snoop Dogg - Gz and Hustlas
- The Notorious B.I.G. - Ready To Die
- [Loon-e-Toon, Mike T - Inglewoodz Finest](https://genius.com/Loon-e-toon-and-dj-mike-tee-inglewoodz-finast-lyrics)
- The Coup - Dig It!
- E-40 - Captain Save a Hoe
- Mobb Deep - Intro

### 1994
- 69 Boyz - Tootsee Roll
- Nas - Represent :: http://www.youtube.com/watch?v=QKYkDwzi-FI
- Nas - The world is yours (Tip mix) released on 2013 re-release
- [The Notorious B.I.G. - Big Poppa](http://genius.com/The-notorious-big-big-poppa-lyrics)
- Craig Mack / LL Cool J / Busta Rhymes / Biggie / Ra - Flava In Ya Ear (Remix)
- [RBL Possee - On the grind](https://genius.com/Young-cellski-on-the-grind-lyrics)
- Scarface - I seen a Man Die
- The Coup - Takin' These
- The Coup - The Name Game
- E-40 - Captain Save a Hoe (Remix)
- Master P - Something For The Street
- Gena Cide - Waste Uva Cular
- [Craig Mack - Flava in Ya Ear (Remix)](https://genius.com/Craig-mack-flava-in-ya-ear-remix-lyrics)

### 1995
- Tupac - California Lovin
- 311 - Hive
- Pharcyde - Runnin
- [Lost Boyz - Jeeps, Lex Coups, Bimaz & Benz](http://genius.com/Lost-boyz-jeeps-lex-coups-bimaz-and-benz-lyrics)
- Skillz - Skillz in '95
- BLACK DYNASTY - DEEP EAST OAKLAND (95 STYLE)
- OC Dre - Master Plan :: https://www.youtube.com/watch?v=F3jXT1BmwBU
- E.S.G. "Swangin' And Bangin'"
- Master P - Intro-17 Reasons ("nine diggity diggity fo, diggity five")
- Master P - When They Gone

### 1996
- Underground Kingz - Fuck my car
- Underground Kingz - Outro
- Underground Kingz - Pinky Ring
- Outkast - Decatur Psalm
- Outkast - 13th Floor/Growing Old
- Tupac - Picture me Rollin
- [DJ Screw - Pimp Tha Pen](http://genius.com/Dj-screw-pimp-tha-pen-lyrics)
- Nas - The Message :: http://www.youtube.com/watch?v=MeAsv9ZGvoA
- Jay Z - Brooklyn's Finest
- O.F.T.B. - Check Yo Hood
- Blue Meanies - 4th of July
- Redman - Welcome (Interlude)
- Redman - Pick It Up
- The Notorious B.I.G. - Dangerous MC's feat. Mark Curry, Snoop Dogg & Busta Rhymes

### 1997
- Wu-Tang - Triumph
- [Wu-Tang - Wu-Revolution](http://genius.com/Wu-tang-clan-wu-revolution-lyrics)
- Lil Keke - Still Pimping The Pens
- [Celly Cel - Pop the Trunk](https://genius.com/Celly-cel-pop-the-trunk-lyrics)
- Master P - Weed & Money (Featuring Silkk The Shocker)

### 1998
- Black Star - B Boys Will B Boys
- Peter and the Test Tube Babies - Twenty Years
- Mr. Shadow - 61909 (featuring O.D.M.)
- Rancid - 1998
- Juvenile - Flossin Season
- [Snoop Dogg - Hustle & Ball](http://genius.com/Snoop-dogg-hustle-and-ball-lyrics) :: https://youtu.be/ldIGX3gEvk0?t=3m6s
- [Lil Troy - Wanna Be a Baller](http://genius.com/998075)
- DJ DMD ft. Fat Pat, Lil' Keke "25 Lighters" (1998)
- Master P - Get Your Paper
- Goodie Mob - They Don't Dance No Mo

### 1999
- Juvenille - Back that ass up (video)
- Dr. Dre - Still D.R.E.
- Mobb Deep - It's Mine
- [MF Doom - Rhymes Like Dimes](https://genius.com/Mf-doom-rhymes-like-dimes-lyrics)
- Missy Elliott - Mysterious (Intro)
- Missy Elliott - Stickin' Chickens
- [Guerilla Maab - Still Here](http://genius.com/Guerilla-maab-still-here-lyrics)
- [Project Pat - Choppers](http://genius.com/Project-pat-choppers-lyrics)
- [Will Smith - Will 2K](Will-smith-will-2k-lyrics)
- [Common, Sadat X, Talib Kweli - 1-9-9-9](https://genius.com/Common-1-9-9-9-lyrics) _h/t Dakota_
- The Queers - Theme From Beyond the Valley of the Assfuckers
- Project Pat - North Memphis
- Jedi Mind Tricks - Heavenly Divine
- Busta Rhymes - Outro-The Burial Song
- Juvenile - Ha (Hot Boys Remix)
- Destiny's Child - Say My Name
- The Flatliners - Nihilism in '99

### 2000
- The Notorious B.I.G. - Notorious B.I.G.* (December 7, 1999)
- [Wu Tang Clan - Conditioner](http://genius.com/Wu-tang-clan-conditioner-lyrics)
- Missy Elliott - Beat Biters (June 22, 1999)
- [Missy Elliott - Hot Boyz](https://genius.com/Missy-elliott-hot-boyz-lyrics) _music video_
- [Hot Boys - I Feel](https://genius.com/Hot-boys-i-feel-lyrics) (1999)
- [Method Man & Redman - 1, 2, 1, 2](https://genius.com/Method-man-and-redman-1-2-1-2-lyrics) ( September 28, 1999)
- [UGK - Choppin' Blades](https://genius.com/Ugk-choppin-blades-lyrics)
- Master P - Nobody Moves
### 2001
- [Z-Ro - I Found Me](http://genius.com/Z-ro-i-found-me-lyrics)
- [Z-Ro - Block Bleeder](http://genius.com/Z-ro-block-bleeder-lyrics)

### 2002
- Lil' Keke & Slim Thugg - Dirty (ft. Dj DMD)
- Wu Tang - Soul Power (Black Jungle)
- Big Tymers - Greg Street Countdown (skit)

### 2003
- Eminem - Go to Sleep
- Lil Kim - The Jump Off
- Lil' Keke & Slim Thugg - This How We Do (ft. Late Nite)

### 2004
- [Pastor Troy - About to go down](https://genius.com/Pastor-troy-about-to-go-down-lyrics)

### 2005
- [Bun B - I'm Ballin'](http://genius.com/Bun-b-im-ballin-lyrics)
- [The Screwed Up Click - S.U.C. 4 Da 713](http://genius.com/The-screwed-up-click-suc-4-da-713-lyrics)
- [Trae tha Truth - What it is] intro off the Later Dayz album
- Missy Elliott - On & On

### 2006
- [T.I. - I'm Talkin' To You](http://genius.com/Ti-im-talkin-to-you-lyrics)
- Ice Cube - Why We Thugs (	Bone Thugs-N-Family album )

### 2007
- [Trae the Truth - Picture me Rollin'](https://www.youtube.com/watch?v=OG5-O6pK6CU) _sounds like 2007, not sure_

### 2008
- Black Eyed Peas - Boom Boom Pow 
- [Bun B - Angel in the Sky](http://genius.com/Bun-b-angel-in-the-sky-lyrics)
- Mike Jones - When I See You
- [Jazz Liberatorz ft. Asheru - I am hip hop](https://www.youtube.com/watch?v=aQ3MK-wMS54)

### 2009
- Die Antwoord - Enter the Ninja
- Underground Kingz - Intro
- MF DOOM - Bumpy's Message

### 2010
- [Bun B - Chuuch!!!](http://genius.com/Bun-b-chuuch-lyrics)
- [Bun B - Countin' Money](http://genius.com/Bun-b-countin-money-lyrics)
- [Lil' Keke - Freestyle](https://www.youtube.com/playlist?list=PL46173933CD154485)
- [Trae tha Truth - What it is](http://genius.com/Trae-tha-truth-what-it-is-lyrics)
- Bun B - No Mixtape
- MellowHype - Loaded Featuring Mike G (Master Only)

### 2012
Fat Tony - Hood Party (feat. Kool A.D. and Despot)

### 2013
- [Travis Barker - Cuz I'm Famous](http://genius.com/Travis-barker-cuz-im-famous-lyrics)
- [Falling in Reverse - Alone](http://genius.com/Falling-in-reverse-alone-lyrics) _by far the worst track on this list_

### 2014 
- T.I. - You Can Tell How I Walk
- RJD2(unknown)

### 2015
- Kendrick Lamar - The Blacker the Berry

### 2020
- [Run the Jewels - JU$T](https://genius.com/Run-the-jewels-ju-t-lyrics)
- [Megan Thee Stallion - B.I.T.C.H.](https://genius.com/Megan-thee-stallion-bitch-lyrics)
### 2021
- [Megan Thee Stallion - Thot Shit](https://genius.com/Megan-thee-stallion-thot-shit-lyrics)
